import { request } from '~/plugins/request'
export function login(username, password, code, uuid, tenantId) {
  const data = {
    username,
    password,
    code,
    uuid,
    tenantId
  }
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: '/getInfo',
    method: 'get'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/captchaImage',
    method: 'get'
  })
}
