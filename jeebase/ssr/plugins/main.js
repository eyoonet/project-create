import Vue from 'vue'

import vuexStore from '~/mixin/$u.mixin'
const components = [

]

Vue.prototype.$u = {}
Vue.mixin(vuexStore)
const Element = {
  install (Vue) {
    components.forEach((component) => {
      Vue.component(component.name, component)
    })
  }
}
// 全局事件
Vue.prototype.eventBus = new Vue()
Vue.prototype.BASE_URL = process.env.baseUrl
Vue.use(Element)
