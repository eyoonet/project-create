import Cookies from 'js-cookie'
export default ({ app, store, params }) => {
  app.router.beforeEach((to, from, next) => {
    if (process.client) {
      console.log('process.client', process.client, 'store.state.app.tenantId', store.getters.tenantId, to)
      if (!Cookies.get('tenantId') && store.getters.tenantId !== undefined) {
        Cookies.set('tenantId', store.getters.tenantId)
      } else {
        to.params.appId && store.dispatch('app/GetConfig', to.params.appId).then(() => {
          Cookies.set('tenantId', store.getters.tenantId)
        })
      }
    }
    next()
  })
}
