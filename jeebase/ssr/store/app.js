export const state = () => ({
  config: {}
})

export const mutations = {
  SET_CONFIG(state, value) {
    state.config = value
  }
}

export const actions = {

}
