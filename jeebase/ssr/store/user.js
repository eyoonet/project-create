import { login, getInfo, logout } from '~/api/login'
import { setToken, removeToken } from '~/utils/auth'
const baseURL = process.env.NODE_ENV === 'development' ? 'http://localhost:3000/dev-api' : 'http://localhost:3000/prod-api'
export const state = () => ({
  isLogin: false,
  token: '',
  name: '',
  avatar: '',
  roles: [],
  permissions: [],
  userId: null,
  userName: null
})

export const mutations = {
  SET_IS_LOGIN(state, value) {
    state.isLogin = value
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_PERMISSIONS: (state, permissions) => {
    state.permissions = permissions
  },
  SET_USER_ID: (state, userId) => {
    state.userId = userId
  },
  SET_USERNAME: (state, username) => {
    state.userName = username
  }
}

export const actions = {
  Login({ commit }, userInfo) {
    const username = userInfo.username.trim()
    const password = userInfo.password
    const code = userInfo.code
    const uuid = userInfo.uuid
    return new Promise((resolve, reject) => {
      login(username, password, code, uuid).then((res) => {
        setToken(res.token)
        commit('SET_TOKEN', res.token)
        commit('SET_IS_LOGIN', true)
        resolve()
      }).catch((error) => {
        reject(error)
      })
    })
  },
  // 获取用户信息
  GetInfo({ commit }) {
    return new Promise((resolve, reject) => {
      getInfo().then((res) => {
        const user = res.user
        const avatar = user.avatar === '' ? require('~/assets/friend.jpg') : baseURL + user.avatar
        if (res.roles && res.roles.length > 0) { // 验证返回的roles是否是一个非空数组
          commit('SET_ROLES', res.roles)
          commit('SET_PERMISSIONS', res.permissions)
        } else {
          commit('SET_ROLES', ['ROLE_DEFAULT'])
        }
        commit('SET_NAME', user.nickName)
        commit('SET_TENANT', user.tenant)
        commit('SET_AVATAR', avatar)
        commit('SET_USER_ID', user.userId)
        commit('SET_IS_LOGIN', true)
        commit('SET_USERNAME', user.userName)
        resolve(res)
      }).catch((error) => {
        reject(error)
      })
    })
  },
  // 退出系统
  LogOut({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        commit('SET_TOKEN', '')
        commit('SET_ROLES', [])
        commit('SET_PERMISSIONS', [])
        commit('SET_IS_LOGIN', false)
        removeToken()
        resolve()
      }).catch((error) => {
        reject(error)
      })
    })
  },
  // 前端 登出
  FedLogOut({ commit }) {
    return new Promise((resolve) => {
      commit('SET_IS_LOGIN', false)
      commit('SET_TOKEN', '')
      removeToken()
      resolve()
    })
  }
}
