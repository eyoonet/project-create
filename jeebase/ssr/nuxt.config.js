export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'ssr',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/main',
    '@/plugins/request',
    '@/plugins/routerBeforeEach',
    '@/plugins/baidu.js'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/content
    '@nuxt/content'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/'
  },

  env: {
    baseUrl: process.env.NODE_ENV === 'development' ? '/dev-api' : '/prod-api',
    FILE_DOMAIN: 'https://admin.szwangjiao.com/prod-api'
  },

  proxy: {
    '/dev-api': {
      target: 'http://localhost:8082',
      // 把 /dev-api 替换成 /
      pathRewrite: {
        '^/dev-api': '/',
        changeOrigin: true // 表示是否跨域
      }
    },
    '/prod-api': {
      target: 'http://localhost:8082',
      // 把 /dev-api 替换成 /
      pathRewrite: {
        '^/prod-api': '/',
        changeOrigin: true // 表示是否跨域
      }
    }
  },
  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
