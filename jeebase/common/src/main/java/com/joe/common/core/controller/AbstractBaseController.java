package com.joe.common.core.controller;

import com.joe.common.core.domain.AjaxResult;
import com.joe.common.core.service.BaseService;
import com.joe.common.utils.bean.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author eyoonet
 */
public abstract class AbstractBaseController extends BaseController implements BaseService {


    protected AjaxResult success(Object data){
        return AjaxResult.success(data);
    }

    protected AjaxResult error(String message, Object data) {
        return AjaxResult.error(message,data);
    }

    protected AjaxResponse response(){
        return new AjaxResponse();
    }

    /**
     * 简单的 pojo 转换
     * @author Joe Fai <Wx=CCTV96>
     * @param list
     * @param targetClass 参数
     * @return {@link List<TARGET>}
     * @Date   2021/11/17 14:16
     */
    protected <TARGET,SOURCE> List<TARGET> simpleConvert(List<SOURCE> list, Class<TARGET> targetClass) {
        List<TARGET> resp = new ArrayList<>(list.size());
        for (SOURCE source : list) {
            resp.add(simpleConvert(source,targetClass));
        }
        return resp;
    }
    /**
     * 简单的 pojo 转换
     * @author Joe Fai <Wx=CCTV96>
     * @param source
     * @param targetClass 参数
     * @return {@link TARGET}
     * @Date   2021/11/17 14:17
     */
    protected <TARGET,SOURCE> TARGET simpleConvert(SOURCE source, Class<TARGET> targetClass) {
        try {
            TARGET target = targetClass.newInstance();
            BeanUtils.copyBeanProp(target, source);
            return target;
        }catch (Exception ignored) {
            throw exception("doToVo error");
        }
    }


    /**
     * 支持链式附加属性
     * 控制器调用示例  return response().attach("attr","ss").success();
     * ajax 返回类型增强
     * @author Joe Fai <Wx=CCTV96>
     * @Date   2021/11/17 17:12
     */
    static class AjaxResponse {

        private Map<String,Object> attrs;

        public AjaxResult success(){
            AjaxResult ajax = AjaxResult.success();
            ajax.putAll(attrs);
            return ajax;
        }

        public AjaxResult success(String msg){
            AjaxResult ajax = AjaxResult.success(msg);
            ajax.putAll(attrs);
            return ajax;
        }

        public AjaxResult success(Object data) {
            AjaxResult ajax = AjaxResult.success(data);
            ajax.putAll(this.attrs);
            return ajax;
        }

        public AjaxResult success(String msg, Object data){
            AjaxResult ajax = AjaxResult.success(msg,data);
            ajax.putAll(this.attrs);
            return ajax;
        }


        public AjaxResult error(){
            AjaxResult ajax = AjaxResult.error();
            ajax.putAll(attrs);
            return ajax;
        }


        public AjaxResult error(String msg){
            AjaxResult ajax = AjaxResult.error(msg);
            ajax.putAll(attrs);
            return ajax;
        }


        public AjaxResult error(String msg, Object data){
            AjaxResult ajax = AjaxResult.error(msg,data);
            ajax.putAll(attrs);
            return ajax;
        }

        public AjaxResponse attach(String attr, Object value) {
            attrs.put(attr,value);
            return this;
        }
    }

}
