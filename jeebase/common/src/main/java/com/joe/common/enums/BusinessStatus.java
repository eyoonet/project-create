package com.joe.common.enums;

/**
 * 操作状态
 * 
 * @author joe
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
