package com.joe.common.enums;

/**
 * 数据源
 * 
 * @author joe
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
