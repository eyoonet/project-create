@echo off  & color 0A 
setlocal enabledelayedexpansion

echo 输入项目名称:
set /p project=

::gitee 下载
git clone https://gitee.com/y_project/RuoYi-Vue.git %cd%/%project%

cd %project%
rmdir /q /s .git
echo %cd%

::指定起始文件夹
set DIR="%cd%"
echo DIR=%DIR%

::文件夹重命名去掉前缀
for /f %%i in ('dir /b ruoyi-*') do (
    set "str=%%i"
    echo 重命名%%i  to !str:ruoyi-=!
    rename %%i !str:ruoyi-=!
)

::src下包名改名
for /f %%a in ('dir /s /b ruoyi') do (
    echo 移动 %%a  to %%~dpajoe
    move %%a %%~dpajoe
)

:: 参数 /R 表示需要遍历子文件夹,去掉表示不遍历子文件夹
:: %%f 是一个变量,类似于迭代器,但是这个变量只能由一个字母组成,前面带上%%
:: 括号中是通配符,可以指定后缀名,*.*表示所有文件
for /R %DIR% %%f in (*.java *.xml *.sql *.yml *.js *.vue .env.* *.vm) do ( 
    echo %%f
    ..\sed -i "s/com.ruoyi/com.joe/g" %%f
    ..\sed -i "s/author ruoyi/author joe/g" %%f
    ..\sed -i "s/ruoyi-//g" %%f
    ..\sed -i "s/ruoyi/joe/g" %%f
    ..\sed -i "s/若依/JeeBase/g" %%f
    ..\sed -i "s/RuoYi/JeeBase/g" %%f
)

::重命名 RuoYiServletInitializer.java RuoYiApplication.java RuoYiConfig.java
for /f %%a in ('dir /s /b RuoYi**.java') do (
    set "str=%%a"
    echo 移动 %%a  to !str:RuoYi=JeeBase!
    move %%a !str:RuoYi=JeeBase!
)

::重命名 ruoyi.scss ruoyi.js
for /f %%a in ('dir /s /b ruoyi**.js ruoyi**.scss') do (
    set "str=%%a"
    echo 移动 %%a  to !str:ruoyi=joe!
    move %%a !str:ruoyi=joe!
)
rename ry.bat joe.bat
rename ry.sh joe.sh

pause