package com.joe.lms.service;

import com.joe.common.exception.ServiceException;
import com.joe.common.utils.spring.SpringUtils;

/**
 * @author eyoonet
 */
public interface BaseService {

    /**
     * 服务层调用
     * @author Joe Fai <Wx=CCTV96>
     * @param cls 参数
     * @return {@link T}
     * @Date   2021/11/17 15:02
     */
    default <T> T call(Class<T> cls){
       return SpringUtils.getBean(cls);
    }

    /**
     * 抛出异常
     * @author Joe Fai <Wx=CCTV96>
     * @param msg 参数
     * @return ServiceException
     * @Date   2021/11/17 15:26
     */
    default ServiceException exception(String msg){
        return new ServiceException(msg);
    }
}
